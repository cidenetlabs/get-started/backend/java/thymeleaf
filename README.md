# Thymeleaf

Demo con JPA postgres y Thymeleaf

## Ejecución del proyecto

Utilizando Java17 desplegar el proyecto:
puedes utilizar tu IDE, [Docker](#Usando_Docker) o desde la terminal (mvn para Windows, mvnw para Linux)
```shell
./mvnw spring-boot:run
```

navegar a la ruta:
```text
http://localhost:8080/?name=myName
```

## Usando Docker
construir la imágen de desarrollo:
```shell
docker build -f Dockerfile.dev -t demo/thy:dev .
```

iniciar el contenedor de desarrollo:
```shell
docker run -it -v ./:/code -p 8080:8080 demo/thy:dev mvn spring-boot:run
```